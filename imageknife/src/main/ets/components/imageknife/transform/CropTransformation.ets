/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BaseTransform } from "../transform/BaseTransform"
import { AsyncTransform } from "../transform/AsyncTransform"
import { Constants } from "../constants/Constants"
import { RequestOption } from "../../imageknife/RequestOption"
import { TransformUtils } from "../transform/TransformUtils"
import image from "@ohos.multimedia.image"

export class CropTransformation implements BaseTransform<PixelMap> {
  private static TAG: string = "CropCircleTransformation";
  private mWidth: number;
  private mHeight: number;
  private mCropType: CropType = CropType.CENTER;

  constructor(width: number, height: number, cropType?: CropType) {
    this.mWidth = width;
    this.mHeight = height;
    this.mCropType = cropType;
  }

  getName() {
    return CropTransformation.TAG + ";mWidth:" + this.mWidth
    + ";mHeight:" + this.mHeight
    + ";mCropType:" + this.mCropType;
  }

  transform(buf: ArrayBuffer, request: RequestOption, func?: AsyncTransform<PixelMap>) {
    if (!buf || buf.byteLength <= 0) {
      console.log(Constants.PROJECT_TAG + ";CropTransformation buf is empty");
      if (func) {
        func(Constants.PROJECT_TAG + ";CropTransformation buf is empty", null);
      }
      return;
    }
    var imageSource = image.createImageSource(buf as any);
    TransformUtils.getPixelMapSize(imageSource, (error, size: {
      width: number,
      height: number
    }) => {
      if (!size) {
        func(error, null)
        return;
      }
      var pixelMapWidth = size.width;
      var pixelMapHeight = size.height;

      this.mWidth = this.mWidth == 0 ? pixelMapWidth : this.mWidth;
      this.mHeight = this.mHeight == 0 ? pixelMapHeight : this.mHeight;

      var scaleX = this.mWidth / pixelMapWidth;
      var scaleY = this.mHeight / pixelMapHeight;
      var scale = Math.max(scaleX, scaleY);

      var scaledWidth = scale * pixelMapWidth;
      var scaledHeight = scale * pixelMapHeight;
      var left = (this.mWidth - scaledWidth) / 2;
      var top = Math.abs(this.getTop(pixelMapHeight));
      var options = {
        editable: true,
        desiredRegion: {
          size: {
            width: scaledWidth > pixelMapWidth ? pixelMapWidth : scaledWidth,
            height: scaledHeight > pixelMapHeight ? pixelMapHeight : scaledHeight
          },
          x: left < 0 ? 0 : left,
          y: top < 0 ? 0 : top,
        },
      }
      imageSource.createPixelMap(options)
        .then((data) => {
          func("", data);
        })
        .catch((e) => {
          console.log(Constants.PROJECT_TAG + ";error:" + e);
          func(e, null);
        })
    })
  }

  private getTop(scaledHeight: number): number{
    switch (this.mCropType.valueOf()) {
      case CropType.TOP.valueOf():
        return 0;
      case CropType.CENTER.valueOf():
        return (this.mHeight - scaledHeight) / 2;
      case CropType.BOTTOM.valueOf():
        return this.mHeight - scaledHeight;
      default:
        return 0;
    }
  }
}

export enum CropType {
  TOP,
  CENTER,
  BOTTOM
}