/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BaseTransform } from "../transform/BaseTransform"
import { AsyncTransform } from "../transform/AsyncTransform"
import { Constants } from "../constants/Constants"
import { RequestOption } from "../../imageknife/RequestOption"

import image from "@ohos.multimedia.image"

export class CropSquareTransformation implements BaseTransform<PixelMap> {
  private static TAG: string = "CropSquareTransformation";

  getName() {
    return CropSquareTransformation.TAG + ";CropSquareTransformation:" + this;
  }

  transform(buf: ArrayBuffer, request: RequestOption, func?: AsyncTransform<PixelMap>) {
    if (!buf || buf.byteLength <= 0) {
      console.log(Constants.PROJECT_TAG + ";CropSquareTransformation buf is empty");
      if (func) {
        func(Constants.PROJECT_TAG + ";CropSquareTransformation buf is empty", null);
      }
      return;
    }
    this.squareCrop(buf, request, func);
  }

  squareCrop(buf: ArrayBuffer, request: RequestOption, func?: AsyncTransform<PixelMap>) {
    var imageSource = image.createImageSource(buf as any);
    imageSource.getImageInfo()
      .then((p) => {
      var pw = p.size.width;
      var ph = p.size.height;
      var outWidth = request.size.width;
      var outHeight = request.size.height;
      if (pw < outWidth) {
        outWidth = pw;
      }
      if (ph < outHeight) {
        outHeight = ph;
      }
      var targetSize = outWidth > outHeight ? outHeight : outWidth;
      var options = {
        editable: true,
        rotate: 0,
        desiredSize: {
          width: outWidth,
          height: outHeight
        },
        desiredRegion: { size: { width: targetSize, height: targetSize },
          x: pw / 2 - targetSize / 2,
          y: ph / 2 - targetSize / 2,
        },
      }
      imageSource.createPixelMap(options)
        .then(data => {
        if (func) {
          func("", data);
        }
      })
        .catch(e => {
        if (func) {
          func(Constants.PROJECT_TAG + ";CropSquareTransformation e:" + e, null);
        }
      })
    })
      .catch((error) => {
      if (func) {
        func(Constants.PROJECT_TAG + ";CropSquareTransformation error:" + error, null);
      }
    })
  }
}