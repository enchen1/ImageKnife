/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BaseTransform } from "../transform/BaseTransform"
import { AsyncTransform } from "../transform/AsyncTransform"
import { Constants } from "../constants/Constants"
import { RequestOption } from "../../imageknife/RequestOption"
import image from "@ohos.multimedia.image"

/**
 * Applies a simple sepia effect.
 * <p>
 * The intensity with a default of 1.0.
 */
export class SepiaFilterTransformation implements BaseTransform<PixelMap> {
  getName() {
    return "SepiaFilterTransformation";
  }

  async transform(buf: ArrayBuffer, request: RequestOption, func?: AsyncTransform<PixelMap>) {
    if (!buf || buf.byteLength <= 0) {
      console.log(Constants.PROJECT_TAG + ";SepiaFilterTransformation buf is empty");
      if (func) {
        func(Constants.PROJECT_TAG + ";SepiaFilterTransformation buf is empty", null);
      }
      return;
    }
    var imageSource = image.createImageSource(buf as any);

    let imageInfo = await imageSource.getImageInfo();
    let size = {
      width: imageInfo.size.width,
      height: imageInfo.size.height
    }

    if (!size) {
      func(new Error("SepiaFilterTransformation The image size does not exist."), null)
      return;
    }

    var pixelMapWidth = size.width;
    var pixelMapHeight = size.height;
    var targetWidth = request.size.width;
    var targetHeight = request.size.height;
    if (pixelMapWidth < targetWidth) {
      targetWidth = pixelMapWidth;
    }
    if (pixelMapHeight < targetHeight) {
      targetHeight = pixelMapHeight;
    }

    var options = {
      editable: true,
      desiredSize: {
        width: targetWidth,
        height: targetHeight
      }
    }
    let data = await imageSource.createPixelMap(options);

    let bufferData = new ArrayBuffer(data.getPixelBytesNumber());
    let bufferNewData = new ArrayBuffer(data.getPixelBytesNumber());
    await data.readPixelsToBuffer(bufferData);

    var dataArray = new Uint8Array(bufferData);
    var dataNewArray = new Uint8Array(bufferNewData);

    for (let index = 0; index < dataArray.length; index += 4) {
      const r = dataArray[index];
      const g = dataArray[index+1];
      const b = dataArray[index+2];
      const f = dataArray[index+3];

      dataNewArray[index+2] = this.checkVisAble(this.colorBlend(this.noise()
        , (r * 0.272) + (g * 0.534) + (b * 0.131)
        , b));
      dataNewArray[index+1] = this.checkVisAble(this.colorBlend(this.noise()
        , (r * 0.349) + (g * 0.686) + (b * 0.168)
        , g));
      dataNewArray[index] = this.checkVisAble(this.colorBlend(this.noise()
        , (r * 0.393) + (g * 0.769) + (b * 0.189)
        , r));
      dataNewArray[index+3] = f;
    }

    await data.writeBufferToPixels(bufferNewData);
    if (func) {
      func("", data);
    }
  }

  private checkVisAble(input: number) {
    if (input > 255) {
      input = 255;
    }
    if (input <= 0) {
      input = 0;
    }
    return input;
  }

  private colorBlend(scale: number, dest: number, src: number): number {
    return (scale * dest + (1.0 - scale) * src);
  }

  private noise(): number {
    return Math.random() * 0.5 + 0.5;
  }
}