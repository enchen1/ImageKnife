/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { DiskLruCache } from "@ohos/disklrucache"
import { LruCache } from "../cache/LruCache"
import {EngineKeyFactories} from "../cache/key/EngineKeyFactories"
import {RequestOption} from "../imageknife/RequestOption"
import {AsyncCallback} from "../imageknife/interface/asynccallback"
import {PlaceHolderManager} from "../imageknife/holder/PlaceHolderManager"
import {ErrorHolderManager} from "../imageknife/holder/ErrorHolderManager"
import {RequestManager} from "../imageknife/requestmanage/RequstManager"
import {NONE} from "../cache/diskstrategy/enum/NONE"
import {FileTypeUtil} from '../imageknife/utils/FileTypeUtil'
import {DownloadClient} from '../imageknife/networkmanage/DownloadClient'
import {IDataFetch} from '../imageknife/networkmanage/IDataFetch'
import {ParseResClient} from '../imageknife/resourcemanage/ParseResClient'
import {IResourceFetch} from '../imageknife/resourcemanage/IResourceFetch'
import {ImageKnifeData} from '../imageknife/ImageKnifeData'
import {FileUtils} from '../cache/FileUtils'
import {FileReader} from '../cache/FileReader'
import image from "@ohos.multimedia.image"
import featureAbility from '@ohos.ability.featureAbility';
import {CompressBuilder} from "../imageknife/compress/CompressBuilder"

export class ImageKnife {
  static readonly SEPARATOR: string = '/'
  private imageKnifeContext;
  private memoryCache: LruCache<string, any>;
  private diskMemoryCache: DiskLruCache;
  private dataFetch: IDataFetch;
  private resourceFetch: IResourceFetch;
  private filesPath: string = ""; // data/data/包名/files目录


  private placeholderCache: string = "placeholderCache"
  private runningRequest: Array<RequestOption>;
  private pendingRequest: Array<RequestOption>;
  private fileTypeUtil: FileTypeUtil; // 通用文件格式辨别
  private diskCacheFolder: string = "ImageKnifeDiskCache"
  private svgAndGifFolder: string = "svgAndGifFolder"; // svg和gif的文件路径地址
  private svgAndGifCommitFile: string = "svgAndGifCommitFile" // svg和gif提交记录

  private defaultListener: AsyncCallback<ImageKnifeData>; // 全局监听器

  private constructor(imgCtx) {
    this.imageKnifeContext = imgCtx;

    // 构造方法传入size 为保存文件个数
    this.memoryCache = new LruCache<string, any>(100);

    // 创建disk缓存 传入的size 为多少比特 比如20KB 传入20*1024
    this.diskMemoryCache = DiskLruCache.create(this.imageKnifeContext.filesDir + ImageKnife.SEPARATOR + this.diskCacheFolder, 30 * 1024 * 1024);

    // 创建网络下载能力
    this.dataFetch = new DownloadClient();

    // 创建本地数据解析能力
    this.resourceFetch = new ParseResClient();

    // 初始化本地 文件保存
    this.filesPath = this.imageKnifeContext.filesDir;
    this.initSvgAndGifEnvironment();

    this.runningRequest = new Array();
    this.pendingRequest = new Array();

    // 通用文件格式识别初始化
    this.fileTypeUtil = new FileTypeUtil();
  }

  getMemoryCache(): LruCache<string, any>{
    return this.memoryCache;
  }

  public static with(context): ImageKnife{
    if (!this.sInstance) {
      this.sInstance = new ImageKnife(context);
    }
    return this.sInstance;
  }

  getDiskMemoryCache(): DiskLruCache{
    return this.diskMemoryCache;
  };

  setDiskMemoryCache(diskLruCache: DiskLruCache) {
     this.diskMemoryCache = diskLruCache;
  };

  getFileTypeUtil(): FileTypeUtil{
    return this.fileTypeUtil;
  }



  getSvgAndGifFolder(): string{
    return this.svgAndGifFolder;
  }

  setSvgAndGifFolder(folderPath: string){
     this.svgAndGifFolder = folderPath;
  }

  getImageKnifeContext() {
    return this.imageKnifeContext;
  }

  setMemoryCache(lrucache: LruCache<string, any>) {
    this.memoryCache = lrucache;
  }

  getDefaultListener() {
    return this.defaultListener;
  }

  private initSvgAndGifEnvironment() {
    let folderExist = FileUtils.getInstance().existFolder(this.filesPath + "/" + this.svgAndGifFolder)
    let fileExist =
      FileUtils.getInstance()
        .exist(this.filesPath + "/" + this.svgAndGifFolder + "/" + this.svgAndGifCommitFile)
    if (folderExist && fileExist) {
      // 创建完成,需要删除上次使用的文件
      var fileReader = new FileReader(this.filesPath + "/" + this.svgAndGifFolder + "/" + this.svgAndGifCommitFile)
      var line: string = ''
      while (!fileReader.isEnd()) {
        line = fileReader.readLine()
        line = line.replace('\n', "").replace('\r', "")
        FileUtils.getInstance().deleteFile(this.filesPath + "/" + this.svgAndGifFolder + "/" + line)
      }
      FileUtils.getInstance().clearFile(this.filesPath + "/" + this.svgAndGifFolder + "/" + this.svgAndGifCommitFile)
    } else {
      if (!folderExist) {
        FileUtils.getInstance().createFolder(this.filesPath + "/" + this.svgAndGifFolder);
      }
      if (!fileExist) {
        FileUtils.getInstance().createFile(this.filesPath + "/" + this.svgAndGifFolder + "/" + this.svgAndGifCommitFile)
      }
    }

  }

  private static sInstance: ImageKnife;

  setDefaultListener(newDefaultListener: AsyncCallback<ImageKnifeData>) {
    this.defaultListener = newDefaultListener;
  }

  public compressBuilder(): CompressBuilder{
    return new CompressBuilder();
  }

  // 替代原来的LruCache
  public replaceLruCache(size:number){
    if(this.memoryCache.map.size() <= 0) {
      this.memoryCache = new LruCache<string, any>(size);
    }else{
      let newLruCache = new LruCache<string, any>(size);
      this.memoryCache.foreachLruCache(function (value, key, map) {
         newLruCache.put(key, value);
      })
      this.memoryCache = newLruCache;
    }
  }

  public replaceDataFetch(fetch:IDataFetch){
    this.dataFetch = fetch;
  }

  // 替代原来的DiskLruCache
  public replaceDiskLruCache(size:number) {
    //    this.diskMemoryCache = DiskLruCache.create(this.imageKnifeContext.filesDir+ImageKnife.SEPARATOR+this.diskCacheFolder, size);

    if (this.diskMemoryCache.getCacheMap().size() <= 0) {
      this.diskMemoryCache = DiskLruCache.create(this.imageKnifeContext.filesDir + ImageKnife.SEPARATOR + this.diskCacheFolder, size);
    } else {
      let newDiskLruCache = DiskLruCache.create(this.imageKnifeContext.filesDir + ImageKnife.SEPARATOR + this.diskCacheFolder, size);
      this.diskMemoryCache.foreachDiskLruCache(function (value, key, map) {
        newDiskLruCache.set(key, value);
      })
      this.diskMemoryCache = newDiskLruCache;
    }
  }

  // 预加载 resource资源一级缓存，string资源实现二级缓存
  preload(request: RequestOption) {
    // 每个request 公共信息补充
    request.setFilesPath(this.filesPath);
    // svg特殊处理
    request._svgAndGifFolder = this.svgAndGifFolder;
    request._svgAndGifCommitFile = this.svgAndGifCommitFile;
    return this.parseSource(request);
  }

  // 正常加载
  call(request: RequestOption) {
    // 添加全局监听
    if(this.defaultListener) {
      request.addListener(this.defaultListener)
    }

    // 每个request 公共信息补充
    request.setFilesPath(this.filesPath);

    // svg特殊处理
    request._svgAndGifFolder = this.svgAndGifFolder;
    request._svgAndGifCommitFile = this.svgAndGifCommitFile;

    // 首先执行占位图 解析任务
    if (request.placeholderSrc) {
      PlaceHolderManager.execute(request)
    }

    // 其次解析错误占位图
    if (request.errorholderSrc) {
      ErrorHolderManager.execute(request)
    }
    return this.parseSource(request);
  }

  loadResources(request: RequestOption) {
    let factories;
    let cacheKey;
    let transferKey;
    let dataKey;
    factories = new EngineKeyFactories();
    // 生成内存缓存key 内存 变换后磁盘

    cacheKey = factories.buildCacheKey(request);

    // 生成磁盘缓存变换后数据key 变换后数据保存在磁盘

    transferKey = factories.buildResourceKey(request);

    // 生成磁盘缓存源数据key 原始数据保存在磁盘

    dataKey = factories.buildDataKey(request);

    request.generateCacheKey = cacheKey;
    request.generateResourceKey = transferKey;
    request.generateDataKey = dataKey;

    this.loadCacheManager(request);
  }

  // 删除执行结束的running
  removeRunning(request: RequestOption) {
    let index = -1;
    for (let i = 0; i < this.runningRequest.length; i++) {
      let tempRunning = this.runningRequest[i];
      if (this.keyEqual(request, tempRunning)) {
        // 如果key相同 说明目前有任务正在执行，我们记录下当前request 放入pendingRunning
        index = i;
        break;
      }
    }
    if (index >= 0) {
      let request = this.runningRequest.splice(index, 1)[0];
      this.loadNextPending(request);
    }
  }

  // 执行相同key的pending队列请求
  keyEqualPendingToRun(index:number){
   let nextPending = this.pendingRequest.splice(index, 1)[0];
      this.runningRequest.push(nextPending)
      RequestManager.execute((nextPending as RequestOption), this.memoryCache, this.diskMemoryCache, this.dataFetch, this.resourceFetch)
  }

  searchNextKeyToRun(){
   // 其次则寻找pending中第一个和running不重复的key
      let index2 = -1;
      for (let i = 0; i < this.pendingRequest.length; i++) {
        let temppending = this.pendingRequest[i];
        let hasKeyEqual = false;
        for (let j = 0; j < this.runningRequest.length; j++) {
          let temprunning = this.runningRequest[j];
          if (this.keyEqual(temppending, temprunning)) {
            hasKeyEqual = true;
            break;
          }
        }
        if (!hasKeyEqual) {
          index2 = i;
        }
      }
      if (index2 >= 0) {
        let nextPending = this.pendingRequest.splice(index2, 1)[0];
        this.runningRequest.push(nextPending)
        RequestManager.execute((nextPending as RequestOption), this.memoryCache, this.diskMemoryCache, this.dataFetch, this.resourceFetch)
      } else {
        // 不执行
      }
  }


  // 加载下一个key的请求
  loadNextPending(request) {
    // 首先寻找被移除key相同的request
    let index = -1;
    for (let i = 0; i < this.pendingRequest.length; i++) {
      let temppending = this.pendingRequest[i];
      if (this.keyEqual(request, temppending)) {
        // 如果key相同 说明目前有任务正在执行，我们记录下当前request 放入pendingRunning
        index = i;
        break;
      }
    }
    if (index >= 0) {
        this.keyEqualPendingToRun(index);
    } else {
        this.searchNextKeyToRun();
    }
  }

  // 启动新线程 去磁盘取 去网络取
  loadCacheManager(request: RequestOption) {
    if (this.keyNotEmpty(request)) {
      let hasRunningRequest = false;
      for (let i = 0; i < this.runningRequest.length; i++) {
        let tempRunning = this.runningRequest[i];
        if (this.keyEqual(request, tempRunning)) {

          // 如果key相同 说明目前有任务正在执行，我们记录下当前request 放入pendingRunning
          hasRunningRequest = true;
          break;
        }
      }
      if (hasRunningRequest) {
        this.pendingRequest.push(request);
      } else {
        this.runningRequest.push(request);
        // 不存在相同key的 任务可以并行
        RequestManager.execute(request, this.memoryCache, this.diskMemoryCache, this.dataFetch, this.resourceFetch)
      }
    }
    else {
      console.log("key没有生成无法进入存取！")
    }


  }

  private keyNotEmpty(request: RequestOption): boolean{
    if (
    request.generateCacheKey != null && request.generateCacheKey.length > 0 &&
    request.generateDataKey != null && request.generateDataKey.length > 0 &&
    request.generateResourceKey != null && request.generateResourceKey.length > 0
    ) {
      return true;
    }
    return false;
  }

  private keyEqual(request1: RequestOption, request2: RequestOption): boolean{
    if (
    request1.generateCacheKey == request2.generateCacheKey &&
    request1.generateResourceKey == request2.generateResourceKey &&
    request1.generateDataKey == request2.generateDataKey
    ) {
      return true;
    }
    return false;
  }

  parseSource(request: RequestOption) {
    if ((typeof (request.loadSrc as image.PixelMap).isEditable) == 'boolean') {
      let imageKnifeData = new ImageKnifeData();
      imageKnifeData.imageKnifeType = ImageKnifeData.PIXELMAP
      imageKnifeData.imageKnifeValue = request.loadSrc as PixelMap
      request.loadComplete(imageKnifeData);
    } else
      if (typeof request.loadSrc == 'string') {
      // 进入三级缓存模型
      return this.loadResources(request);
    } else {
      let res = request.loadSrc as Resource;
      if (typeof res.id != 'undefined' && typeof res.type != 'undefined') {
        //进入三级缓存模型  本地资源不参与磁盘缓存
        let none = new NONE();
        request.diskCacheStrategy(none);
        this.loadResources(request);
      } else {
        console.error("输入参数有问题！")
      }
    }
  }
}



