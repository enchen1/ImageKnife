/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { CropCallback } from './CropCallback'
import image from "@ohos.multimedia.image"

export namespace Crop {

  export function crop(buf: ArrayBuffer, x: number, y: number, cropWidth: number, cropHeight: number, func?: CropCallback<PixelMap>, colorRatio?: number) {
    if (!buf || buf.byteLength <= 0) {
      console.log("Crop buf is empty");
      if (func) {
        func("Crop buf is empty", null);
      }
      return;
    }
    var imageSource = image.createImageSource(buf as any);
    getPixelMapSize(imageSource, (error, size: {
      width: number,
      height: number
    }) => {
      if (!size) {
        func(error, null)
        return;
      }
      var pixelMapWidth = size.width;
      var pixelMapHeight = size.height;
      if (x < 0 || x > pixelMapWidth) {
        func("Crop error x must be less than pixelMapWidth ", null)
        return;
      }
      if (y < 0 || y > pixelMapHeight) {
        func("Crop error x must be less than pixelMapHeight ", null)
        return;
      }
      var options = {
        editable: true,
        rotate: 0,
        desiredSize: {
          width: pixelMapWidth,
          height: pixelMapHeight
        },
        desiredRegion: { size: { width: cropWidth, height: cropHeight },
          x: x,
          y: y,
        },
      }
      imageSource.createPixelMap(options)
        .then((data) => {
          if (colorRatio && colorRatio <= 1) {
            colorRatioPixelMap(data, pixelMapWidth, pixelMapHeight, colorRatio, func);
          } else {
            func("", data);
          }
        })
        .catch((e) => {
          func(e, null);
        })
    })
  }

  async function colorRatioPixelMap(data: any, width: number, height: number, colorRatio: number, func?: CropCallback<PixelMap>) {
    if (!data) {
      func("colorRatio pixelMap is null", null);
      return;
    }
    if (colorRatio > 1) {
      throw new Error("the colorRatio must be <= 1");
    }
    var buffer = new ArrayBuffer(width * height * 5);
    var bytes = new Uint8Array(buffer);
    var buffer1B = new ArrayBuffer(width * height * 5);
    var bytes1B = new Uint8Array(buffer1B);

    let readPromise = data.readPixelsToBuffer(buffer)
    await readPromise;
    for (let i = 0;i < bytes.length; i++) {
      bytes1B[i] = bytes[i] * colorRatio;
    }
    let writePromise = data.writeBufferToPixels(buffer1B)
    await writePromise;
    func("", data);
  }


  function getPixelMapSize(imageSource: any, func: CropCallback<{
    width: number,
    height: number
  }>) {
    if (!imageSource) {
      return;
    }
    imageSource.getImageInfo((err, value) => {
      if (err) {
        func(err, null)
        return;
      }
      var pWidth = value.size.width;
      var pHeight = value.size.height;
      func('', { width: pWidth, height: pHeight });
    })
  }
}