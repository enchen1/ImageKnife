# ImageKnife

**专门为OpenHarmony打造的一款图像加载缓存库，致力于更高效、更轻便、更简单。**

## 简介

本项目基于开源库 [Glide](https://github.com/bumptech/glide) 进行OpenHarmony的自研版本：

- 支持内存缓存，使用LRUCache算法，对图片数据进行内存缓存。
- 支持磁盘缓存，对于下载图片会保存一份至磁盘当中。
- 支持进行图片变换,支持自定义变换。
- 支持用户配置参数使用:(例如：配置是否开启第一级内存缓存，配置磁盘缓存策略，配置仅使用缓存加载数据，配置图片变换效果，配置占位图，配置加载失败占位图等)。
- 推荐使用ImageKnifeComponent组件配合ImageKnifeOption参数来实现功能。
- 支持用户自定义配置实现能力参考ImageKnifeComponent组件中对于入参ImageKnifeOption的处理。

<img src="screenshot/g3.gif" width="100%"/>

## 下载安装

```typescript
npm install @ohos/imageknife --save
```

OpenHarmony npm环境配置等更多内容，参考安装教程 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md)。

## 使用说明

1.首先初始化全局ImageKnife实例，在AbilityStage.ts中调用ImageKnife.with(this.context)进行初始化

```typescript
import AbilityStage from "@ohos.application.AbilityStage"
import {ImageKnife} from '@ohos/imageknife'

export default class MyAbilityStage extends AbilityStage {
    onCreate() {
        globalThis.ImageKnife = ImageKnife.with(this.context);
    }
}
```

2.参考[推荐使用](###'推荐使用')或[自定义实现](###'自定义实现')

#### 推荐使用：

使用ImageKnifeOption作为入参，配合自定义组件ImageKnifeComponent使用。

```typescript
@Entry
@Component
struct Index {
  @State imageKnifeOption1: ImageKnifeOption =
    {
      loadSrc: $r('app.media.jpgSample'),
      size: { width: 300, height: 300 },
      placeholderSrc: $r('app.media.icon_loading'),
      errorholderSrc: $r('app.media.icon_failed'),
    };


  build() {
    Scroll() {
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        ImageKnifeComponent({ imageKnifeOption: $imageKnifeOption1 })
      }
    }
    .width('100%')
    .height('100%')
  }
}
```

#### 自定义实现：

使用原生Image组件，配合用户配置参数实现。

##### 步骤1：

定义自定义类。

```typescript
export default class PixelMapPack{
   pixelMap:PixelMap;
}
```

使用@State关联PixelMapPack。

```typescript
@State  imageKnifePixelMapPack:PixelMapPack = new PixelMapPack();
width:number = 200;
height:number = 200;
```

##### 步骤2：

在你的component组件中，写下一个Image组件，将基础参数（入参PixelMap，组件的宽、高）配置好。

```typescript
Image(this.imageKnifePixelMapPack.pixelMap)
        .backgroundColor(Color.Grey)
        .objectFit(ImageFit.Contain)
        .width(this.width)
        .height(this.height)
```

##### 步骤3：

在aboutToAppear() 函数中调用加载流程。

```typescript
//配置参数
let requestOptin = new RequestOption();    
   //加载本地图片
    requestOptin.load($r('app.media.jpgSample'))
    .addListener((err,data) => {
      //加载成功/失败回调监听
        if (data.isPixelMap()) {
        let pixelMapPack = new PixelMapPack();
        pixelMapPack.pixelMap = data.imageKnifeValue as PixelMap;
        //当前的PixelMap资源发生变化，jpgSample图片将会由Image组件重新渲染，并展示  
        this.imageKnifePixelMapPack = pixelMapPack;
      }
    })
    .placeholder( $r('app.media.icon_loading'), (data)=>{
        // 占位图回调监听
      })
   .errorholder( $r('app.media.icon_failed'), (data)=>{
        // 失败占位图回调监听
      })
     .thumbnail(0.3, (data) => {
        // 缩略图加载成功回调
    })
      // 一定要把控件大小传给RequestOption,图片变换必须
      .setImageViewSize({width:200, height:200})
      // 设置缓存策略
     .diskCacheStrategy(new AUTOMATIC())
     .addProgressListener((percentValue: string) => {
        // 图片网络加载进度条百分比回调
      })
     .addRetryListener((error: any) => {
           // 加载失败重试监听 图片加载失败时优先展示重试图层，点击重试图层，会重新进行一次加载流程
      })
      // 左上圆角10pixel像素点
    .roundedCorners({top:10})
    // 启动加载流程，执行结果将会返回到上面的回调接口中
    globalThis.ImageKnife.call(requestOptin);
```

##### 步骤4：

更多细节设置请参考自定义Component的ImageKnifeComponent实现。

## 接口说明

### RequestOption 用户配置参数

| 方法名                                                       | 入参                                                       | 接口描述                                                 |
| ------------------------------------------------------------ | ---------------------------------------------------------- | -------------------------------------------------------- |
| load(src: string \| PixelMap \| Resource)                    | string \| PixelMap \| Resource                             | 待加载图片的资源                                         |
| setImageViewSize(imageSize: {   width: number,   height: number }) | {   width: number,   height: number }                      | 传入显示图片组件的大小，变换的时候需要作为参考           |
| diskCacheStrategy(strategy: DiskStrategy)                    | DiskStrategy                                               | 配置磁盘缓存策略  NONE SOURCE RESULT  ALL  AUTOMATIC     |
| placeholder(src: PixelMap \| Resource, func?: AsyncSuccess<PixelMap>) | src: PixelMap \| Resource, func?: AsyncSuccess<PixelMap>   | 配置占位图，其中func为数据回调函数                       |
| errorholder(src: PixelMap \| Resource, func?: AsyncSuccess<PixelMap>) | src: PixelMap \| Resource, func?: AsyncSuccess<PixelMap>   | 配置加载失败占位图，其中func为数据回调函数               |
| addListener(func: AsyncCallback<PixelMap>)                   | func: AsyncCallback<PixelMap>                              | 配置整个监听回调，数据正常加载返回，加载失败返回错误信息 |
| thumbnail(sizeMultiplier:number, func?: AsyncSuccess<ImageKnifeData>) | sizeMultiplier:number, func?: AsyncSuccess<ImageKnifeData> | 设置缩略图比例，缩略图返回后，加载并展示缩略图           |
| addProgressListener(func?: AsyncSuccess<string>){   this.progressFunc = func;   return this; } | func?: AsyncSuccess<string>                                | 设置网络下载百分比监听，返回数据加载百分比数值           |
| addRetryListener(func?: AsyncSuccess<any>){   this.retryFunc = func;   return this; } | func?: AsyncSuccess<any>                                   | 设置重试监听                                             |
| addAllCacheInfoCallback(func: IAllCacheInfoCallback)         | func: IAllCacheInfoCallback                                | 设置获取所有缓存信息监听                                 |
| skipMemoryCache(skip: boolean)                               | skip: boolean                                              | 配置是否跳过内存缓存                                     |
| retrieveDataFromCache(flag: boolean)                         | flag: boolean                                              | 配置仅从缓存中加载数据                                   |

同时支持[图片变换相关](##'图片变换相关')接口。

### ImageKnife 启动器/门面类

| 方法名                          | 入参                   | 接口描述                           |
| ------------------------------- | ---------------------- | ---------------------------------- |
| call(request: RequestOption)    | request: RequestOption | 根据用户配置参数具体执行加载流程   |
| preload(request: RequestOption) | request: RequestOption | 根据用户配置参数具体执行预加载流程 |

### 缓存策略相关

| 使用方法                                   | 类型      | 策略描述                                 |
| ------------------------------------------ | --------- | ---------------------------------------- |
| request.diskCacheStrategy(new ALL())       | ALL       | 表示既缓存原始图片，也缓存转换过后的图片 |
| request.diskCacheStrategy(new AUTOMATIC()) | AUTOMATIC | 表示尝试对本地和远程图片使用最佳的策略   |
| request.diskCacheStrategy(new DATA())      | DATA      | 表示只缓存原始图片                       |
| request.diskCacheStrategy(new NONE())      | NONE      | 表示不缓存任何内容                       |
| request.diskCacheStrategy(new RESOURCE())  | RESOURCE  | 表示只缓存转换过后的图片                 |

### 图片变换相关

| 使用方法                       | 类型                               | 相关描述                                             |
| ------------------------------ | ---------------------------------- | ---------------------------------------------------- |
| request.centerCrop()           | CenterCrop                         | 可以根据图片文件，目标显示大小，进行对应centerCrop   |
| request.centerInside()         | CenterInside                       | 可以根据图片文件，目标显示大小，进行对应centerInside |
| request.fitCenter()            | FitCenter                          | 可以根据图片文件，目标显示大小，进行对应fitCenter    |
| request.blur()                 | BlurTransformation                 | 模糊处理                                             |
| request.brightnessFilter()     | BrightnessFilterTransformation     | 亮度滤波器                                           |
| request.contrastFilter()       | ContrastFilterTransformation       | 对比度滤波器                                         |
| request.cropCircle()           | CropCircleTransformation           | 圆形剪裁显示                                         |
| request.cropCircleWithBorder() | CropCircleWithBorderTransformation | 圆环展示                                             |
| request.cropSquare()           | CropSquareTransformation           | 正方形剪裁                                           |
| request.crop()                 | CropTransformation                 | 自定义矩形剪裁                                       |
| request.grayscale()            | GrayscaleTransformation            | 灰度级转换                                           |
| request.invertFilter()         | InvertFilterTransformation         | 反转滤波器                                           |
| request.pixelationFilter()     | PixelationFilterTransformation     | 像素化滤波器                                         |
| request.rotateImage()          | RotateImageTransformation          | 图片旋转                                             |
| request.roundedCorners()       | RoundedCornersTransformation       | 圆角剪裁                                             |
| request.sepiaFilter()          | SepiaFilterTransformation          | 乌墨色滤波器                                         |
| request.sketchFilter()         | SketchFilterTransformation         | 素描滤波器                                           |
| request.mask()                 | MaskTransformation                 | 遮罩                                                 |
| request.swirlFilter()          | SwirlFilterTransformation          | 扭曲滤波器                                           |

## 兼容性

支持 OpenHarmony API version 9 及以上版本。

## 目录结构

```
/imageknife/src/
- main/ets/components
    - cache               # 缓存相关内容
       - diskstrategy    # 缓存策略
       - key           # 缓存key生成策略
       - Base64.ets      # Base64算法
       - CustomMap.ets       # 自定义Map封装
       - DiskCacheEntry.ets# 磁盘缓存entry
       - DiskLruCache.ets  # 磁盘LRU缓存策略
       - FileReader.ets   # 文件读取相关
       - FileUtils.ets       # 文件工具类
       - LruCache.ets    # 内存LRU缓存策略
       - Md5.ets        # MD5算法
       
    - imageknife               # imageknife主要内容
       - compress       # 压缩相关
       - constants          # 常量相关
       - entry             # 部分数据结构
       - holder         # 占位图相关解析
       - interface          # 接口相关
       - networkmanage       # 网络相关
       - pngj          # pngj相关
       - requestmanage       # imageknife请求相关
       - resourcemanage   # 本地资源解析相关
       - transform          # 图片变换相关
       - utils             # 工具类相关
       - ImageKnife.ets         # imageknife门面，app持久化类
       - ImageKnifeData.ets      # 数据封装
       - ImageKnifeComponent.ets  # 自定义控件封装
       - ImageKnifeOption.ets # 用户传参数封装
       - PixelMapPack.ets # PixelMap封装
       - RequestOption.ets # 用户设置参数封装
       
/entry/src/
- main/ets/MainAbility     
    - pages               # 测试page页面列表
       - basicTestFeatureAbilityPage.ets     # 测试元能力
       - basicTestFileIOPage.ets           # 测试fileio
       - basicTestMediaImage.ets           # 测试媒体image
       - basicTestResourceManagerPage.ets    # 测试本地资源解析
       - compressPage.ets                # 压缩页面
       - cropImagePage.ets                # 裁剪页面
       - cropImagePage2.ets                # 手势裁剪页面
       - frescoImageTestCasePage.ets        # 测试属性动画组件切换
       - frescoLayerTestCasePage.ets        # 测试ImageKnifeComponent组件切换配合属性动画
       - frescoRetryTestCasePage.ets        # 测试ImageKnifeComponent加载失败重试
       - index.ets                         # 测试页面入口
       - indexFresco.ets                 # 二级测试页面入口
       - loadNetworkTestCasePage.ets        # 网络加载测试
       - loadResourceTestCasePage.ets       # 本地加载测试
       - showErrorholderTestCasePage.ets     # 加载失败占位图测试
       - storageTestDiskLruCache.ets        # 磁盘缓存测试
       - storageTestLruCache.ets           # 内存缓存测试
       - testAllCacheInfoPage.ets          # 所有缓存信息获取测试
       - testAllTypeImageKnifeComponentPage.ets      # 所有类型图片加载测试
       - testAllTypeNativeImagePage.ets      # 所有类型本地图片加载测试
       - testGifDontAnimatePage.ets         # gif加载静态图片测试
       - testImageKnifeOptionChangedPage.ets     # ImageKnifeOption数据切换测试
       - testImageKnifeOptionChangedPage2.ets        # ImageKnifeOption数据切换测试
       - testMultiThreadWorkerPage2.ets      # 多线程测试
       - testPlaceholderPage.ets           # 加载占位图测试
       - testPreloadPage.ets              # 预加载测试
       - testResourceManagerPage.ets        # 解析本地资源测试
       - transformPixelMapPage.ets          # 所有类型变换测试
       - transformTestCasePage.ets          # 所有类型变换配合ImageKnifeComponent测试
       
    - workers             # 测试worker多线程
       - worker1.js         # worker多线程测试
```

## 贡献代码

使用过程中发现任何问题都可以提 [issue](https://gitee.com/openharmony-tpc/ImageKnife/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-tpc/ImageKnife/issues) 。

## 开源协议

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-tpc/ImageKnife/blob/master/LICENSE) ，请自由的享受和参与开源。

## 遗留问题

1.目前只支持一种图片变换效果。

2.目前svg和gif动图不支持变换效果。